
SSH (Secure Shell) and Public/Private Key Pairs Notes
=====================================================

[SSH Wiki](https://en.wikipedia.org/wiki/Secure_Shell)

When you start working with multiple linux systems, you will most likely need to use SSH to access a remote linux system.  The system you are currently logged into is called you "host machine" and you can use SSH to login into a remote machine.

SSH Keys
--------

When you need to login to a remote system, there are two ways to accomplish the authentication.  One way is to provide the username and password for the remote system.

However, when you need to execute multiple commands to a remote system, it is inconvenient to manually type in the username and password every time.  This is where a public/private key pair comes in.

Generate Public/Private Keys
----------------------------

[Generate Keys](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)

These instructions are a sub-set of the instructions above.  Please follow the GitLab notes if you with for more detail.

Generate RSA Keys:

    ssh-keygen -o -t rsa -b 4096 -C "email@example.com"

When you generate and SSH key, there is an option to add a passphrase.  This is similar to having a credit card with a pin or 2-factor authentication.  You need to have the key file but you also need to know the passphrase to use the key.

It is recommended that you add a passphrase to the key so that if someone got a hold of your key file, they wouldn't be able to use it without the passphrase as well.

Using SSH Keys
--------------

When you are using SSH keys and it has a passphrase, you will need to type in the passphrase in each time unless you start an ssh-agent daemon and add the key to it.

This project has a bash script that will help you start the ssh-agent.

    $ ./bin/start_ssh_agent

Once the ssh-agent is running, then you can add the key to the ssh-agent daemon using the following command.  Once this happens, you will not need to type in the passphrase anymore as long as the daemon is running.

    $ ~/.ssh/id_rsa



