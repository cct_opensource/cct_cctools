
Networking Tools
================


Determine which program is using port:

	$ sudo lsof -i :80

[Resource](https://stackoverflow.com/questions/4421633/who-is-listening-on-a-given-tcp-port-on-mac-os-x#4421674
)
On macOS High Sierra, use this command:

lsof -nP -i4TCP:$PORT | grep LISTEN
On older versions, use one of the following forms:

lsof -nP -iTCP:$PORT | grep LISTEN
lsof -nP -i:$PORT | grep LISTEN
Substitute $PORT with the port number or a comma-separated list of port numbers.

Prepend sudo (followed by a space) if you need information on ports below #1024.

The -n flag is for displaying IP addresses instead of host names. This makes the command execute much faster, because DNS lookups to get the host names can be slow (several seconds or a minute for many hosts).

The -P flag is for displaying raw port numbers instead of resolved names like http, ftp or more esoteric service names like dpserve, socalia.

See the comments for more options.


Forward SSH ports:

https://apple.stackexchange.com/questions/98331/can-i-connect-to-a-local-smb-share

SMB on local file system:
-------------------------

Finder identifies smb://127.0.0.1 as a local filesystem and refuses
to mount port 445 even if you've set up an SSH tunnel using

-L 445:localhost:445 
The solution is to alias your lo0 interface to 127.0.0.2 and Finder doesn't see this as a local address.

sudo ifconfig lo0 127.0.0.2 alias up 
Create the ssh tunnel and map to your Samba / CIFS server

ssh user@remote -L 127.0.0.2:445:smbhost:445 
Now you can open the share in Finder

smb://user@127.0.0.2