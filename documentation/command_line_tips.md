
vi Tips
=======

To cut a line in vi, you can use the `dd` command.  If you then want to paste it somewhere else, move the cursor
to the location where you want to paste and press `p`.


SSH Keys
========

Generate ssh key

	$ ssh-keygen -t rsa

Start the ssh agent

	$ eval "$(ssh-agent -s)"

