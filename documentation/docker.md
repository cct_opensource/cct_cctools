
Creative Collisions Docker Documentation
========================================

### Image cleanup.  

Quite often you will end up with dangling docker images hanging around from previous runs.  These take up space on your hard drive.  To clean them up you can use the following command.

	$ docker rmi $(docker images --filter "dangling=true" -q --no-trunc)

I believe this removes docker containers that have exited?

	$ docker rm -v $(docker ps --filter status=exited -q)

You can remove an individual image using the following:

	$ docker rmi <image name>


 