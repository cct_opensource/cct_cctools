
Linux Tips and Tricks
=====================

When you are working with mounted drives and NFS, you may come across a case where there there stale file handlers.

mount.nfs: Stale file handler

To clear this situation, you will need to unmount the drive using this command.

    $ umount -l <mounted location>