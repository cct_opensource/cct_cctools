
Password Hashing and Storage
============================

[Storing Passwords Resource](https://www.cyberciti.biz/python-tutorials/securely-hash-passwords-in-python/)

The first important thing to note is that passwords should be stored hashed, not encrypted. For the readers without a background in cryptography, the important distinction between hashing and encryption is that encryption is a reversible operation while hashing isn’t.

Linux Password Hashing
----------------------

[Update Password Hashing](https://www.cyberciti.biz/faq/rhel-centos-fedora-linux-upgrading-password-hashing/)

There is a Linux command called "makepasswd"
[makepasswd Docker Image](https://github.com/atmoz/makepasswd)

You can use the above Docker image to 

	$ echo -n "your-password" | docker run -i --rm atmoz/makepasswd --crypt-md5 --clearfrom=-

Use openssl to generate Linux password:

	$ openssl passwd -1 -salt xyz yourpass


Shadow Utils
------------

Shadow utils is a package in Linux that's installed by default in most of the distributions, used for separating passwords from /etc/passwd. After implementing shadow-utils, passwords are now saved in /etc/shadow file in Linux. This /etc/shadow file is only accessible by root.

You can see that unlike the /etc/passwd file the /etc/shadow file only has the "r" (read) permission set for root user. Which means no other user has access to this file.


$1$Etg2ExUZ$F9NTP7omafhKIlqaBMqng1

 The above shown encoded hash value can be further classified into three different fields as below.

1. The first field is a numerical number that tell's you the hashing algorithm that's being used.

 $1 = MD5 hashing algorithm.
 $2 = Blowfish Algorithm is in use.
 $2a = eksblowfish Algorithm
 $5 = SHA-256 Algorithm
 $6 = SHA-512 Algorithm

### How to Display hashing Algorthm used in your Linux Machine?

 [root@localhost ~]# authconfig --test| grep hashing
 password hashing algorithm is md5

Above command clearly shows that, at present the algorithm used by your Linux machine is md5(Which will be used for all the user's by default).


How to generate a shadow style password hash with MD5-based password algorithm:

	$ openssl passwd -1 redhat123

### Blowfish Algorithm

There are two different prefixes for the blowfish hashing algorithm, 2a and 2b.


Linux Password Authentication
-----------------------------

https://en.wikipedia.org/wiki/Linux_PAM

Python Password Modules
-----------------------

[Passlib for MD5](https://passlib.readthedocs.io/en/stable/lib/passlib.hash.md5_crypt.html)

