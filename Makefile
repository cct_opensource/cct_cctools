
# Makefile for Creative Collisions Tech Tools
# Copyright 2019 Creative Collisions Technology, LLC J. Patrick Farrell

INSTALL_DIR=~/bin

install:
	mkdir -p ${INSTALL_DIR}
	cp -a bin/* ${INSTALL_DIR}
	make -C docker install